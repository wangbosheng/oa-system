import * as localforage from 'localforage';

localforage.config({
    driver: localforage.WEBSQL,
    name: 'cache',
    version: 1.0,
    size: 4294967296,
    storeName: 'keyvaluepairs',
    description: 'some description'
});

export const STORAGE_KEY = 'questionnaire';

/**
 * @function Set storage
 * @param name
 * @param content
 * @param maxAge
 */
export const setStore = (name, content, maxAge = null) => {
    if (!global.window || !name) {
        return;
    }

    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }

    var storage = global.window.localStorage;

    try {
        storage.setItem(name, content);
        localforage.setItem(name, content);
    } catch (error) {
        console.log(error);
    }

    try {
        var timeout = parseInt(new Date().getTime() / 1000);
        if (maxAge && !isNaN(parseInt(maxAge))) {
            storage.setItem(`${name}_expire`, timeout + maxAge);
            localforage.setItem(`${name}_expire`, timeout + maxAge);
        }
    } catch (error) {
        console.log(error);
    }
};

/**
 * @function Get storage
 * @param name
 * @returns {*}
 */
export const getStore = name => {
    if (!global.window || !name) {
        return;
    }

    let content = window.localStorage.getItem(name);
    let _expire = window.localStorage.getItem(`${name}_expire`);

    if (_expire) {
        let now = parseInt(new Date().getTime() / 1000);
        if (now > _expire) {
            return;
        }
    }

    try {
        return JSON.parse(content);
    } catch (e) {
        return content;
    }
};

/**
 * @function clearStore storage
 * @param name
 */
export const clearStore = (name) => {
    if (!global.window || !name) {
        return;
    }
    window.localStorage.removeItem(name);
    window.localStorage.removeItem(`${name}_expire`);
};


/**
 * @function clearDB storage
 * @param name
 */
// export const clearDB = (name) => {
//     localforage.removeItem(name);
//     localforage.removeItem(`${name}_expire`);
// };

/**
 * @function Clear all storage
 */
export const clearAll = (name) => {
    if (!global.window || !name) {
        return;
    }
    if (name == 'all') {
        localforage.clear();
        window.localStorage.clear();
    } else if (name == 'storage') {
        window.localStorage.clear();
    } else if (name == 'db') {
        localforage.clear();
    } else {
        window.localStorage.clear();
    }
};


/**
 * @function 获取缓存信息
 * @param {*} key 
 */
export const get = (key = STORAGE_KEY) => {
    return JSON.parse(window.localStorage.getItem(key));
};

/**
 * @function 设置缓存信息
 * @param {*} items 
 * @param {*} key 
 */
export const save = (items, key = STORAGE_KEY) => {
    window.localStorage.setItem(key, JSON.stringify(items));
};


/**
 * @function Set storage
 * @param name
 * @param content
 * @param maxAge
 */
export const setStoreAll = async(name, content, maxAge = null) => {

    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }

    try {
        localforage.setItem(name, content);
    } catch (error) {
        console.log(error);
    }

    try {
        localStorage.setItem(name, content);
    } catch (error) {
        console.log(error);
    }

    if (maxAge && !isNaN(parseInt(maxAge))) {

        let timeout = parseInt(new Date().getTime() / 1000);

        try {
            localforage.setItem(`${name}_expire`, timeout + maxAge);
        } catch (error) {
            console.log(error);
        }

        try {
            localStorage.setItem(`${name}_expire`, timeout + maxAge);
        } catch (error) {
            console.log(error);
        }
    }
};



/**
 * @function Get storage
 * @param name
 * @returns {*}
 */
export const getStoreAll = async(name) => {

    let content = await localforage.getItem(name);
    let _expire = await localforage.getItem(`${name}_expire`);

    if (content == null || typeof content == 'undefined') {
        content = localStorage.getItem(name);
    }

    if (_expire == null || typeof _expire == 'undefined') {
        _expire = localStorage.getItem(`${name}_expire`);
    }

    if (_expire) {
        let now = parseInt(new Date().getTime() / 1000);
        if (now > _expire) {
            return;
        }
    }

    try {
        return JSON.parse(content);
    } catch (e) {
        return content;
    }
};

/**
 * @function Set storage
 * @param name
 * @param content
 * @param maxAge
 */
export const setStoreDB = async(name, content, maxAge = null) => {

    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }

    try {
        localforage.setItem(name, content);
    } catch (error) {
        console.log(error);
    }

    if (maxAge && !isNaN(parseInt(maxAge))) {

        let timeout = parseInt(new Date().getTime() / 1000);

        try {
            localforage.setItem(`${name}_expire`, timeout + maxAge);
        } catch (error) {
            console.log(error);
        }

    }
};

/**
 * @function Get storage
 * @param name
 * @returns {*}
 */
export const getStoreDB = async(name) => {

    let content = await localforage.getItem(name);
    let _expire = await localforage.getItem(`${name}_expire`);

    if (_expire) {
        let now = parseInt(new Date().getTime() / 1000);
        if (now > _expire) {
            return;
        }
    }

    try {
        if (typeof content === 'string') {
            content = JSON.parse(content);
        }
    } catch (error) {
        console.log(error);
    }

    try {
        return content;
    } catch (e) {
        return content;
    }

};

/**
 * @function Clear storage
 * @param name
 */
export const clearDB = name => {
    if (name == 'all') {
        localforage.clear();
    } else {
        localforage.removeItem(name);
        localforage.removeItem(`${name}_expire`);
    }
};

/**
 * @function Clear storage
 * @param name
 */
export const clearAllDB = () => {
    localforage.clear();
};

/**
 * @function clearToken and turn page to login page 
 */
export const clearToken = () => {
    //清空缓存，一般是因为浏览器token有效，但是远程服务器token失效，清空token信息即可
    clearStore('pro__Access-Token');
    clearDB('pro__Access-Token');
    console.log('token reject by remote server , probally caused by token invalid... ');
    window.location.href = `/user/login`;
}