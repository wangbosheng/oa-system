//import Vue from 'vue';
import router from './router';
import store from './store';
import * as types from '@/store/mutation-types';
import * as utils from '@/utils/util';

// NProgress Configuration
try {
    NProgress.configure({
        showSpinner: true
    });
} catch (error) {
    console.log(error);
}

router.beforeEach((to, from, next) => {
    window.handleRouter(to, from, next, NProgress, types, store, utils, router)
});

router.afterEach(() => {
    try {
        NProgress.done();
    } catch (error) {
        console.log(error);
    }
});