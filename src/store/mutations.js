export default {
    //设置登录信息
    SET_LOGGED(state, { tokenList, userInfo }) {
        state.logged = true;
        state.userInfo = userInfo;
    },
    //设置当前用户
    SET_USER(state, data) {
        state.userInfo = data;
    },
    //设置退出登录状态
    SET_LOGOUT(state) {
        state.logged = false;
        state.userInfo = null;
    },
    //设置主题
    SET_THEME(state, theme) {
        state.theme = theme
    },
    //设置pageLoading
    PAGE_LOADING(state, status) {
        state.pageLoading = status
    },
    //设置windowLoading
    WINDOW_LOADING(state, status) {
        state.windowLoading = status
    },
    //设置系统信息
    SET_SYSTEM(state, data) {
        state.system = data;
    },
    //设置公司组织架构信息
    SET_ORGANIZATION_LIST(state, data) {
        state.organizationList = data;
    },
    //设置当前公司
    SET_CURRENT_ORGANIZATION(state, data) {
        state.currentOrganization = data;
    },
    //设置Socket执行动作
    catchSocketAction(state, data) {
        state.socketAction = data;
    },
    //设置BoundClient
    SET_BOUND_CLIENT(state, data) {
        state.boundClient = data;
    },
    //初始化数据
    INIT_DATA(state) {
        let data = localStorage.getItem('vue-chat-session');
        if (data) {
            state.sessions = JSON.parse(data);
            state.session = state.sessions.find(item => item.id === state.currentSessionId);
            state.username = state.session.user.name;
        }
        //TODO 设置定时器，每隔一段时间间隔，从消息队列拉去当前用户的消息
        setInterval(() => {

        }, state.interval);
    },
    //发送消息
    SEND_MESSAGE(state, content) {
        let session = state.sessions.find(item => item.id === state.currentSessionId);
        let others = state.sessions.filter(session => session.id !== state.currentSessionId);
        state.sessions = [session, ...others];
        state.session.messages.push({
            content: content,
            date: new Date(),
            self: true
        });

        //发送信息到消息队列
    },
    //选择会话
    SELECT_SESSION(state, id) {
        let session = state.sessions.find(item => item.id === id);
        state.currentSessionId = id;
        state.session = session;
        state.username = session.user.name;
    },
    //搜索
    SET_FILTER_KEY(state, value) {
        state.filterKey = value;
    },
    //设置用户头像，用户名称
    SET_CUR_USER(state, value) {
        state.user.name = value.name;
        state.user.img = value.img;
    },
    //设置用户信息
    SET_USER_INFO(state, value) {
        let session = state.sessions.find(item => item.name === value.name);
        session.user.name = value.name;
        session.user.img = value.img;
    },
    //刷新用户名称
    REFRESH_NAME(state, value) {
        state.username = value;
    }
};