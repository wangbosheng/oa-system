import app from './modules/app'
import user from './modules/user'
import permission from './modules/permission'
import getters from './getters'
import state from './state'
import mutations from './mutations'
import actions from './actions'
import common from './modules/common'
import menu from './modules/menu'

try {
    Vue.use(Vuex)
} catch (error) {
    console.log(error);
}

const store = new Vuex.Store({
    modules: {
        app,
        user,
        permission,
        common,
        menu,
    },
    state,
    mutations,
    actions,
    getters
})


store.watch(
    (state) => state.sessions,
    (val) => {
        //当聊天信息发送变化时，自动执行此处，本地缓存聊天信息
        localStorage.setItem('vue-chat-session', JSON.stringify(val));
    }, {
        deep: true
    }
);

export default store;

//定义vchat需要使用的Actions
export const vchatActions = {
    initData: ({ dispatch }) => dispatch('INIT_DATA'),
    sendMessage: ({ dispatch }, content) => dispatch('SEND_MESSAGE', content),
    selectSession: ({ dispatch }, id) => dispatch('SELECT_SESSION', id),
    search: ({ dispatch }, value) => dispatch('SET_FILTER_KEY', value),
    refreshName: ({ dispatch }, username) => dispatch('REFRESH_NAME', username),
    setCurUser: ({ dispatch }, userinfo) => dispatch('SET_CUR_USER', userinfo),
    setUserInfo: ({ dispatch }, userinfo) => dispatch('SET_USER_INFO', userinfo)
};