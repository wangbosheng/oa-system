import * as mutationTypes from "@/store/mutation-types"

const getters = {
    device: state => state.app.device,
    theme: state => state.app.theme,
    color: state => state.app.color,
    token: state => state.user.token,
    username: state => state.user.username,
    welcome: state => state.user.welcome,
    permissionList: state => state.user.permissionList,
    addRouters: state => state.permission.addRouters,
    avatar: state => {
        try {
            state.user.avatar = Vue.ls.get(mutationTypes.USER_INFO).avatar;
            return state.user.avatar
        } catch (error) {
            console.log(error);
        }
    },
    nickname: state => {
        try {
            state.user.realname = Vue.ls.get(mutationTypes.USER_INFO).realname;
            return state.user.realname
        } catch (error) {
            console.error(error);
        }
    },
    userInfo: state => {
        try {
            state.user.info = Vue.ls.get(mutationTypes.USER_INFO);
            return state.user.info
        } catch (error) {
            console.error(error);
        }
    },
}

export default getters