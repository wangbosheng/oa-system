import * as mutationTypes from "@/store/mutation-types"

const app = {
    state: {
        sidebar: {
            opened: true,
            withoutAnimation: false
        },
        device: 'desktop',
        theme: '',
        layout: '',
        contentWidth: '',
        fixedHeader: false,
        fixSiderbar: false,
        autoHideHeader: false,
        color: null,
        weak: false,
        multipage: true //默认多页签模式
    },
    mutations: {
        SET_SIDEBAR_TYPE: (state, type) => {
            state.sidebar.opened = type
            Vue.ls.set(mutationTypes.SIDEBAR_TYPE, type)
        },
        CLOSE_SIDEBAR: (state, withoutAnimation) => {
            Vue.ls.set(mutationTypes.SIDEBAR_TYPE, true)
            state.sidebar.opened = false
            state.sidebar.withoutAnimation = withoutAnimation
        },
        TOGGLE_DEVICE: (state, device) => {
            state.device = device
        },
        TOGGLE_THEME: (state, theme) => {
            Vue.ls.set(mutationTypes.DEFAULT_THEME, theme)
            state.theme = theme
        },
        TOGGLE_LAYOUT_MODE: (state, layout) => {
            Vue.ls.set(mutationTypes.DEFAULT_LAYOUT_MODE, layout)
            state.layout = layout
        },
        TOGGLE_FIXED_HEADER: (state, fixed) => {
            Vue.ls.set(mutationTypes.DEFAULT_FIXED_HEADER, fixed)
            state.fixedHeader = fixed
        },
        TOGGLE_FIXED_SIDERBAR: (state, fixed) => {
            Vue.ls.set(mutationTypes.DEFAULT_FIXED_SIDEMENU, fixed)
            state.fixSiderbar = fixed
        },
        TOGGLE_FIXED_HEADER_HIDDEN: (state, show) => {
            Vue.ls.set(mutationTypes.DEFAULT_FIXED_HEADER_HIDDEN, show)
            state.autoHideHeader = show
        },
        TOGGLE_CONTENT_WIDTH: (state, type) => {
            Vue.ls.set(mutationTypes.DEFAULT_CONTENT_WIDTH_TYPE, type)
            state.contentWidth = type
        },
        TOGGLE_COLOR: (state, color) => {
            Vue.ls.set(mutationTypes.DEFAULT_COLOR, color)
            state.color = color
        },
        TOGGLE_WEAK: (state, flag) => {
            Vue.ls.set(mutationTypes.DEFAULT_COLOR_WEAK, flag)
            state.weak = flag
        },
        SET_MULTI_PAGE(state, multipageFlag) {
            Vue.ls.set(mutationTypes.DEFAULT_MULTI_PAGE, multipageFlag)
            state.multipage = multipageFlag
        }
    },
    actions: {
        setSidebar: ({
            commit
        }, type) => {
            commit('SET_SIDEBAR_TYPE', type)
        },
        CloseSidebar({
            commit
        }, {
            withoutAnimation
        }) {
            commit('CLOSE_SIDEBAR', withoutAnimation)
        },
        ToggleDevice({
            commit
        }, device) {
            commit('TOGGLE_DEVICE', device)
        },
        ToggleTheme({
            commit
        }, theme) {
            commit('TOGGLE_THEME', theme)
        },
        ToggleLayoutMode({
            commit
        }, mode) {
            commit('TOGGLE_LAYOUT_MODE', mode)
        },
        ToggleFixedHeader({
            commit
        }, fixedHeader) {
            if (!fixedHeader) {
                commit('TOGGLE_FIXED_HEADER_HIDDEN', false)
            }
            commit('TOGGLE_FIXED_HEADER', fixedHeader)
        },
        ToggleFixSiderbar({
            commit
        }, fixSiderbar) {
            commit('TOGGLE_FIXED_SIDERBAR', fixSiderbar)
        },
        ToggleFixedHeaderHidden({
            commit
        }, show) {
            commit('TOGGLE_FIXED_HEADER_HIDDEN', show)
        },
        ToggleContentWidth({
            commit
        }, type) {
            commit('TOGGLE_CONTENT_WIDTH', type)
        },
        ToggleColor({
            commit
        }, color) {
            commit('TOGGLE_COLOR', color)
        },
        ToggleWeak({
            commit
        }, weakFlag) {
            commit('TOGGLE_WEAK', weakFlag)
        },
        ToggleMultipage({
            commit
        }, multipageFlag) {
            commit('SET_MULTI_PAGE', multipageFlag)
        }
    }
}

export default app