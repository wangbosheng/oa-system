import { getStore } from "../assets/js/storage";

const userInfo = getStore('userInfo', true);
const theme = getStore('theme');
//定时拉去消息队列信息时间间隔
const interval = 300;
//默认当前用户头像
const cvatar = '//cdn.jsdelivr.net/gh/Miazzy/yunwisdom_cdn@v1.0.0/images/icon-chat-07.png';
//默认其他用户头像
const avatar = '//cdn.jsdelivr.net/gh/Miazzy/yunwisdom_cdn@v1.0.0/images/icon-manage-16.png';

export default {
    //系统主题
    theme: theme ? theme : 'dark',
    //登录状态
    logged: !!userInfo,
    //用户信息
    userInfo: userInfo,
    //能查看的组织列表
    organizationList: getStore('organizationList', true),
    //当前组织
    currentOrganization: getStore('currentOrganization', true),
    //系统配置
    system: getStore('system', true),
    //窗口loading
    windowLoading: true,
    //页面加载loading
    pageLoading: true,
    socketAction: '',
    //是否绑定client
    boundClient: false,
    //当前用户
    user: {
        name: '当前用户',
        img: cvatar
    },
    //会话列表
    sessions: [{
        id: 1,
        user: {
            name: '示例介绍',
            img: avatar
        },
        messages: []
    }],
    //当前选中的会话
    currentSessionId: 1,
    //当前用户名称
    username: '',
    //当前会话
    session: null,
    //过滤出只包含这个key的会话
    filterKey: '',
    //系统间隔
    interval: interval
}