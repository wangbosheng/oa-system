import { Segment, useDefault } from 'segmentit';

const segmentit = useDefault(new Segment());
const numreg = "一二三四五六七八九十1234567890";
const wordreg = ['我们', '他们', '你们'];


/**
 * @function 对文本进行分词处理，获取有效的分词结果
 * @param {*} words 
 */
export const analyzer = (words) => {

    let result = segmentit.doSegment(words);

    result = result.filter((item) => { return !numreg.includes(item.w.slice(0, 1)) && !wordreg.includes(item.w) && item.w.length > 1 });
    result = result.filter((item, index, array) => { let tmpIndex = array.findIndex(element => item.w == element.w); return index == tmpIndex; });
    result = result.sort((one, two) => { return one.p !== two.p ? (two.p - one.p) * 1000 : two.w.localeCompare(one.w) });
    result = result.length > 6 ? result.slice(0, 6) : result;
    result = result.map((item) => { return item.w });

    return result;
}