export async function insert(key, value) {
    try {
        value = JSON.stringify(value);
    } catch (error) {
        console.error(error);
    }
    try {
        return await localforage.setItem(key, value);
    } catch (error) {
        console.error(error);
        return 'error';
    }
}

export async function update(key, value) {
    try {
        value = JSON.stringify(value);
    } catch (error) {
        console.error(error);
    }
    try {
        return await localforage.setItem(key, value);
    } catch (error) {
        console.error(error);
        return 'error';
    }
}

export async function remove(key) {
    try {
        return await localforage.setItem(key, '');
    } catch (error) {
        console.error(error);
        return 'error';
    }
}

export async function query(key) {
    var value = await localforage.getItem(key);
    try {
        value = JSON.parse(value);
    } catch (error) {
        console.error(error);
        return 'error';
    }
    return value;
}