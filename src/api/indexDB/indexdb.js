//import Idb from 'idb-js'; //  引入Idb
import config from './config';

//定义数据库操作对象
var indexDB = null;
var tableName = 'bs_cache';

/**
 * @function 初始化函数
 */
async function init() {
    try {
        if (indexDB == null) {
            indexDB = await Idb(config);
        }
    } catch (error) {
        console.error(error);
    }
    return indexDB;
}

/**
 * @function 插入对象函数
 * @param {*} key
 * @param {*} value
 */
export async function insert(key, value) {
    //获取初始化对象
    var db = await init();

    //如果是对象，则先序列化
    try {
        value = typeof value == 'string' ? JSON.stringify(value) : value;
    } catch (error) {
        console.error(error);
    }

    //执行数据库操作
    try {
        //返回Promise对象
        return new Promise((resolve) => {
            db.insert({
                tableName: tableName,
                data: {
                    key,
                    value,
                },
                success: () => {
                    resolve('success');
                },
            });
        });
    } catch (error) {
        console.error(error);
        return new Promise();
    }
}

/**
 * @function 更新对象函数
 * @param {*} key
 * @param {*} value
 */
export async function update(key, value) {
    //获取初始化对象
    var db = await init();

    //如果是对象，则先序列化
    try {
        value = typeof value == 'string' ? JSON.stringify(value) : value;
    } catch (error) {
        console.error(error);
    }

    //执行数据库操作
    try {
        //返回Promise对象
        return new Promise((resolve) => {
            db.update({
                tableName: tableName,
                condition: (item) => item.key == key,
                handle: (r) => {
                    r.value = value;
                },
                success: (r) => {
                    resolve(r);
                },
            });
        });
    } catch (error) {
        console.error(error);
        return new Promise();
    }
}

/**
 * @function 删除对象函数
 * @param {*} key
 * @param {*} value
 */
export async function remove(key) {
    //获取初始化对象
    var db = await init();

    //执行数据库操作
    try {
        //返回Promise对象
        return new Promise((resolve) => {
            db.delete({
                tableName: tableName,
                condition: (item) => item.key == key,
                success: (res) => {
                    resolve(res);
                },
            });
        });
    } catch (error) {
        console.error(error);
        return new Promise();
    }
}

/**
 * @function 查询对象函数
 * @param {*} key
 */
export async function query(key) {
    //获取初始化对象
    var db = await init();

    //执行数据库操作
    try {
        //返回初始化Promise对象
        return new Promise((resolve) => {
            db.query({
                tableName: tableName,
                condition: (item) => item.key == key,
                success: (r) => {
                    if (r.length > 0) {
                        var obj = r[0].value;
                        try {
                            obj = JSON.parse(obj);
                        } catch (error) {
                            console.error(error);
                        }
                        resolve(obj);
                    } else {
                        resolve('');
                    }
                },
            });
        });
    } catch (error) {
        console.error(error);
        return new Promise();
    }
}