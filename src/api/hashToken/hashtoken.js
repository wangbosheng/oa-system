import * as tools from "@/utils/util";
import * as UUID from 'uuid-js';
import * as md5 from 'js-md5';

/**
 * @function JS Sleep休眠函数
 * @param {*} time 
 */
export const sleep = async(time = 1000) => {
    return new Promise((resolve) => setTimeout(resolve, time));
}

export const queryToken = (date, username) => {
    var key = '';
    var hash = '';
    var stoken = '';
    let fingerprint = '';
    //获取缓存值
    var htoken = localStorage.getItem(`system_${username}_hash_token`);

    //校验缓存token是否有效
    if (!tools.isNull(htoken) && JSON.parse(htoken).date == date) {
        htoken = JSON.parse(htoken);
        key = htoken.key;
        hash = htoken.hash;
        stoken = htoken.stoken;
        fingerprint = htoken.fingerprint;
    }

    //如果未获取到stoken，则从缓存中获取
    if (tools.isNull(stoken)) {
        let accessToken = localStorage.getItem(`pro__Access-Token`);
        stoken = tools.isNull(accessToken) ? '' : JSON.parse(accessToken).value;
    }

    //如果未获取到指纹，则从缓存中获取指纹
    if (tools.isNull(fingerprint)) {
        let temp = localStorage.getItem(`system_fingerprint`);
        fingerprint = tools.isNull(temp) ? '' : temp;
    }

    return { key, hash, stoken, fingerprint };
}

export const calcuToken = (key, hash, date, username) => {

    //计算开始时间戳
    var stimestamp = new Date().getTime();
    var consume = 0;
    var hashinfo = '';

    //设置url token hashkey
    if (!(!tools.isNull(key) && !tools.isNull(hash))) {


        //遍历数据，进行挖矿
        for (var i = 0; i++ < 10000000;) {
            key = (UUID.create().toString() + UUID.create().toString() + UUID.create().toString() + UUID.create().toString()).replace(/-/g, '').substring(0, 128);
            hash = md5(key + date + username);
            if (hash.startsWith('0000')) {
                let accessToken = localStorage.getItem(`pro__Access-Token`);
                let fingerprint = localStorage.getItem(`system_fingerprint`);
                let etimestamp = new Date().getTime();
                let stoken = tools.isNull(accessToken) ? '' : JSON.parse(accessToken).value;
                consume = etimestamp - stimestamp;
                hashinfo = { key, hash, consume, date, username, stoken, fingerprint };
                localStorage.setItem(`system_${username}_hash_token`, JSON.stringify(hashinfo));
                console.log('calculate the hash key :' + key + ' hash:' + hash + ' consume:' + consume);
                break;
            }
        }

    }

    return hashinfo;

}

export const calcuHashToken = (key, hash, data, prefix = '0000') => {

    //计算开始时间戳
    var stimestamp = new Date().getTime();
    var consume = 0;
    var hashinfo = '';

    //设置url token hashkey
    if (!(!tools.isNull(key) && !tools.isNull(hash))) {

        //遍历数据，进行挖矿
        for (var i = 0; i++ < 10000000;) {
            key = (UUID.create().toString() + UUID.create().toString() + UUID.create().toString() + UUID.create().toString()).replace(/-/g, '').substring(0, 128);
            hash = md5(key + data);
            if (hash.startsWith(prefix)) {
                let etimestamp = new Date().getTime();
                consume = etimestamp - stimestamp;
                hashinfo = { key, hash, data, consume };
                break;
            }
        }

    }

    return hashinfo;

}

export const calcuHashTokenFast = (key, hash, data, prefix = '0000') => {

    //计算开始时间戳
    var stimestamp = new Date().getTime();
    var consume = 0;
    var hashinfo = '';
    var mprefix = prefix + prefix + prefix;

    //设置url token hashkey
    if (!(!tools.isNull(key) && !tools.isNull(hash))) {

        //遍历数据，进行挖矿
        for (var i = 0; i++ < 10000000;) {
            key = (UUID.create().toString() + UUID.create().toString() + UUID.create().toString() + UUID.create().toString()).replace(/-/g, '').substring(0, 128);
            hash = md5(key + data);
            if (hash.startsWith(prefix) || hash.endsWith(prefix) || hash.includes(mprefix.slice(0, prefix.length * 1.5))) {
                let etimestamp = new Date().getTime();
                consume = etimestamp - stimestamp;
                hashinfo = { key, hash, data, consume };
                break;
            }
        }

    }

    return hashinfo;

}