import * as requestAPI from '@/utils/request';
import * as storage from '@/utils/storage';
import * as tools from '@/utils/util';
import * as workflowAPI from '@/api/workflow';
import * as authortool from "@/api/blogAPI/author";

try {
    requestAPI.axios.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded';
} catch (e) {
    console.error(e);
}

//设置配置信息
export const api = window.requestAPIConfig || {};

/**
 * @function 发送Post请求 
 * @param {*} url 
 * @param {*} parameter 
 */
export async function postAction(url, parameter) {
    try {
        return requestAPI.axios({
            url: url,
            method: 'post',
            data: parameter,
        });
    } catch (error) {
        console.log(error);
    }
}

/**
 * @function 发送Http请求（method= {post | put}）
 * @param {*} url 
 * @param {*} parameter 
 * @param {*} method 
 */
export async function httpAction(url, parameter, method) {
    try {
        return requestAPI.axios({
            url: url,
            method: method,
            data: parameter,
        });
    } catch (error) {
        console.log(error);
    }
}

/**
 * @function 执行修改请求
 * @param {*} url 
 * @param {*} parameter 
 */
export async function putAction(url, parameter) {
    try {
        return requestAPI.axios({
            url: url,
            method: 'put',
            data: parameter,
        });
    } catch (error) {
        console.log(error);
    }
}

/**
 * @function 执行查询请求
 * @param {*} url 
 * @param {*} parameter 
 */
export async function getAction(url, parameter) {

    try {
        return requestAPI.axios({
            url: url,
            method: 'get',
            params: parameter,
        });
    } catch (error) {
        console.log(error);
    }

}

/**
 * @function 执行删除请求
 * @param {*} url 
 * @param {*} parameter 
 */
export async function deleteAction(url, parameter) {
    try {
        return requestAPI.axios({
            url: url,
            method: 'delete',
            params: parameter,
        });
    } catch (error) {
        console.log(error);
    }
}

/**
 * @function 获取用户列表信息
 * @param {*} parameter 
 */
export async function getUserList(parameter) {
    return await window.getUserList(parameter, requestAPI);
}

/**
 * @function 获取角色信息函数
 */
export async function getRoleList(parameter) {
    return await window.getRoleList(parameter, requestAPI);
}

/**
 * @function 获取服务信息函数
 * @param {*} parameter 
 */
export async function getServiceList(parameter) {
    return await window.getServiceList(parameter, requestAPI);
}

/**
 * @function 获取权限信息函数
 * @param {*} parameter 
 */
export async function getPermissions(parameter) {
    return await window.getPermissions(parameter, requestAPI);
}

/**
 * @function 获取用户信息操作
 */
export function getInfo() {
    return window.getInfo(requestAPI);
}

/**
 * @function 保存操作
 */
export async function saveService(parameter) {
    return await window.saveService(parameter, requestAPI);
}

/**
 * @function 定时刷新Token，保持连接，Keep-Alive
 */
export async function queryToken() {
    return await window.queryToken(storage, tools);
}

/**
 * 下载文件 用于excel导出
 * @param url
 * @param parameter
 * @returns {*}
 */
export async function downFile(url, parameter) {
    return await window.downFile(url, parameter, requestAPI, storage, tools);
}

/**
 * 获取文件访问路径
 * @param avatar
 * @param imgerver
 * @param str
 * @returns {*}
 */
export function getFileAccessHttpUrl(avatar, imgerver, subStr) {
    return window.getFileAccessHttpUrl(avatar, imgerver, subStr);
}

/**
 * 查询URL地址TableID变量
 */
export function queryURLTableParam() {
    return window.queryURLTableParam();
}

/**
 * 查询当前业务对应表单名称
 * @param {*} url
 */
export async function queryTableName(callback) {
    return await window.queryTableName(callback, queryURLTableParam);
}

/**
 * 检测审批是否存在 存在 false  不存在 true
 * @param {*} tableName
 * @param {*} businessID
 */
export async function queryApprovalExist(tableName, businessID) {
    return await window.queryApprovalExist(tableName, businessID);
}

/**
 * 检测审批是否存在 存在 false  不存在 true
 * @param {*} tableName
 * @param {*} businessID
 */
export async function queryApprovalLength(tableName, businessID) {
    return await window.queryApprovalLength(tableName, businessID);
}

/**
 * 添加数据
 * @param {*} tableName
 * @param {*} id
 */
export async function insertTableData(tableName, node) {
    return await window.insertTableData(tableName, node);
}

export async function postTableData(tableName, node) {
    return await window.postTableData(tableName, node);
}

/**
 * 添加数据
 * @param {*} tableName
 * @param {*} id
 */
export async function deleteTableData(tableName, id) {
    return await window.deleteTableData(tableName, id);
}

/**
 * 更新数据
 * @param {*} tableName
 * @param {*} id
 * @param {*} node
 */
export async function patchTableData(tableName, id, node) {
    return await window.patchTableData(tableName, id, node);
}

/**
 * 更新数据
 * @param {*} tableName
 * @param {*} id
 * @param {*} node
 */
export async function patchTableItem(tableName, id, node) {
    return await window.patchTableItem(tableName, id, node);
}

/**
 * 查询数据
 * @param {*} tableName
 * @param {*} id
 */
export async function queryTableData(tableName, id) {
    return await window.queryTableData(tableName, id);
}

/**
 * @function 分页查询表单数据
 */
export async function queryTableDataByParam(
    tableName,
    username,
    realname,
    page,
    size,
    param
) {
    return await window.queryTableDataByParam(tableName, username, realname, page, size, param);
}

/**
 * 查询数据(all)
 * @param {*} tableName
 */
export async function queryTableAll(tableName) {
    return await window.queryTableAll(tableName);
}

/**
 * @function 查询部门信息
 */
export async function queryDepartName(id) {
    return await window.queryDepartName(id);
}

/**
 * @function 查询动态信息(最近十条)
 */
export async function queryDynamic() {
    return await window.queryDynamic();
}

/**
 * @function 查询个人最新动态(最近十条)
 */
export async function queryDynamicByUser(username) {
    return await window.queryDynamicByUser(username, storage);
}

/**
 * @function 查询数据
 * @param {*} tableName
 * @param {*} foreignKey
 * @param {*} id
 */
export async function queryTableDataByField(tableName, field, value) {
    return await window.queryTableDataByField(tableName, field, value);
}

/**
 * @function 查询用户信息
 */
export async function queryUserInfoByView(username) {
    return await window.queryUserInfoByView(username);
}

/**
 * @function 查询数据
 * @param {*} tableName
 * @param {*} foreignKey
 * @param {*} id
 */
export async function queryTableDataAll(tableName) {
    return await window.queryTableDataAll(tableName);
}

/**
 * @function 查询表单字段数据
 * @param {*} tableName
 * @param {*} foreignKey
 * @param {*} id
 */
export async function queryTableFieldInfo(tableName, field, value) {
    return await window.queryTableFieldInfo(tableName, field, value);
}

/**
 * @function 查询流程权责业务信息
 */
export async function queryBusinessInfo(tableName, callback) {
    return await window.queryBusinessInfo(tableName, callback);
}

/**
 * @function 根据数据字典中的节点编号，查询到这个节点对应的员工信息
 */
export async function queryProcessNodeEmployee(node, callback) {
    return await window.queryProcessNodeEmployee(node, callback);
}

/**
 * @function 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function queryProcessNodeProcName(node, callback) {
    return await window.queryProcessNodeProcName(node, callback);
}

/**
 * @function 查询审批处理页面的记录
 */
export async function queryUserList(params) {
    return await window.queryUserList(params);
}

/**
 * @function 查询审批处理页面的记录
 */
export async function queryProcessLogToApproved(username, realname, params) {
    return await window.queryProcessLogToApproved(username, realname, params);
}

/**
 * @function 查询审批历史记录页面的记录
 */
export async function queryProcessLogHisApproved(username, realname, params) {
    return await window.queryProcessLogHisApproved(username, realname, params);
}

/**
 * @function 查询我的待办数据
 */
export async function queryProcessLogWait(
    username,
    realname,
    page = 0,
    size = 50
) {
    return await window.queryProcessLogWait(username, realname, page, size);
}

/**
 * @function 查询用户名称信息
 */
export async function queryUserName() {
    return await window.queryUserName();
}

/**
 * @function 查询用户名称信息
 */
export async function queryUserNameByDB() {
    return await window.queryUserNameByDB();
}

/**
 * @function 查询用户名称信息
 */
export function queryUserNameByCache() {
    return window.queryUserNameByCache();
}

/**
 * @function 检查是否存在流程
 */
export async function queryExistWorkflow(id) {
    return await window.queryExistWorkflow(id);
}

/**
 * 查询工作流程的节点配置(审核节点、审批节点、知会节点)
 */
export async function queryWorkflowNode(id) {
    return await window.queryWorkflowNode(id);
}

/**
 * 查询工作流程的节点配置(审核节点、审批节点、知会节点)
 */
export async function queryWorkflowNodeByUser(tableName, username) {
    return await window.queryWorkflowNodeByUser(tableName, username);
}

/**
 * 查询我的待办数据
 */
export async function queryProcessLogWaitByParam(
    username,
    param,
    page = 0,
    size = 50
) {
    return await window.queryProcessLogWaitByParam(username, param, page, size);
}

/**
 * 查询我的已办数据
 */
export async function queryProcessLogDoneByTime(
    username,
    realname,
    page = 0,
    size = 50,
    time
) {
    return await window.queryProcessLogDoneByTime(username, realname, page, size, time);
}

/**
 * 查询最新的10篇博文数据
 */
export async function queryBlogInfoNew(page = 0, size = 10) {
    return await window.queryBlogInfoNew(page, size);
}

/**
 * 查询我的博文数据
 */
export async function queryBlogInfoByUser(username, page = 0, size = 50) {
    return await window.queryBlogInfoByUser(username, page, size);
}

/**
 * 查询所有博文数据
 */
export async function queryBlogInfo(
    page = 0,
    size = 50,
    username,
    nousername,
    starttime,
    endtime
) {
    return await window.queryBlogInfo(page, size, username, nousername, starttime, endtime);
}

/**
 * 查询我的已办数据
 */
export async function queryProcessLogDone(
    username,
    realname,
    page = 0,
    size = 50
) {
    return await window.queryProcessLogDone(username, realname, page, size);
}

/**
 * 查询我的已办数据
 */
export async function queryProcessLogDoneAll(username, realname) {
    return await window.queryProcessLogDoneAll(username, realname);
}

/**
 * 查询我的待办数据
 */
export async function queryProcessLogWaitAll(username, realname) {
    return await window.queryProcessLogWaitAll(username, realname);
}

/**
 * 查询我的已办数据（条件查询ALL）
 */
export async function queryProcessLogDoneByParamAll(username, param) {
    return await window.queryProcessLogDoneByParamAll(username, param);
}

/**
 * 查询我的待办数据（条件查询ALL）
 */
export async function queryProcessLogWaitByParamAll(username, param) {
    return await window.queryProcessLogWaitByParamAll(username, param);
}

/**
 * 查询我的已办数据（条件查询）
 */
export async function queryProcessLogDoneByParam(
    username,
    param,
    page = 0,
    size = 50
) {
    return await window.queryProcessLogDoneByParam(username, param, page, size);
}

/**
 * 查询审批知会记录页面的记录
 */
export async function queryProcessLogInfApproved(username, realname, params) {
    return await window.queryProcessLogInfApproved(username, realname, params);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function queryProcessLogInfByID(tableName, id) {
    return await window.queryProcessLogInfByID(tableName, id);
}

/**
 * 获取某业务记录对应的审批日志信息
 */
export async function queryProcessLogInformed(tableName, business_data_id) {
    return await window.queryProcessLogInformed(tableName, business_data_id);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function queryProcessLog(tableName, businessID) {
    return await window.queryProcessLog(tableName, businessID);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function queryProcessLogByID(tableName, id) {
    return await window.queryProcessLogByID(tableName, id);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function deleteTableItem(tableName, node) {
    return await window.deleteTableItem(tableName, node);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function deleteProcessLog(tableName, node) {
    return await window.deleteProcessLog(tableName, node);
}

/**
 * 根据数据字典中的节点编号，删除到这个节点对应的流程信息
 */
export async function deleteProcessLogInf(tableName, node) {
    return await window.deleteProcessLogInf(tableName, node);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function postTableItem(tableName, node) {
    return await window.postTableItem(tableName, node);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function postProcessLog(node) {
    return await window.postProcessLog(node, tools);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function postProcessFreeNode(node) {
    return await window.postProcessFreeNode(node, tools);
}

/**
 * 根据数据字典中的节点编号，查询到这个节点对应的流程岗位名称
 */
export async function postProcessLogHistory(node) {
    return await window.postProcessLogHistory(node, tools);
}

/**
 * 想知会记录列表提交数据
 */
export async function postProcessLogInformed(node) {
    return await window.postProcessLogInformed(node, tools);
}

/**
 * 获取某业务记录对应的审批日志信息
 */
export async function queryPRLogHistoryByDataID(business_data_id) {
    return await window.queryPRLogHistoryByDataID(business_data_id, tools);
}

/**
 * 获取行政公告数据
 */
export async function queryAnnounceList(page = 0, size = 50) {
    return await window.queryAnnounceList(page, size, tools);
}

/**
 * 获取红头文件数据
 */
export async function queryHeadList(page = 0, size = 50) {
    return await window.queryHeadList(page, size, tools);
}

/**
 * @function 获取新闻资讯数据
 */
export async function queryNewsList(page = 0, size = 50) {
    return await window.queryNewsList(page, size, tools);
}

/**
 * @function 获取奖罚通报数据
 */
export async function queryNoticeList(page = 0, size = 50) {
    return await window.queryNoticeList(page, size, tools);
}

/**
 * @function 获取市场观察数据
 */
export async function queryViewsList(page = 0, size = 50) {
    return await window.queryViewsList(page, size, tools);
}

/**
 * @function 获取所有公告信息
 */
export async function queryNotifyAll(
    type = 'all',
    size = 50,
    content = '',
    starttime,
    endtime
) {
    return await window.queryNotifyAll(type, size, content, starttime, endtime, queryAnnounceList, queryHeadList, queryNewsList, queryNoticeList, queryViewsList, tools);
}

/**
 * 获取某业务记录对应的审批日志信息
 */
export async function queryPRLogByDataID(business_data_id) {
    return await window.queryPRLogByDataID(business_data_id, tools);
}

/**
 * 获取某业务记录对应的审批日志信息
 */
export async function queryPRLogInfByDataID(business_data_id) {
    return await window.queryPRLogInfByDataID(business_data_id, tools);
}

/**
 * @function 获取某业务记录对应的审批日志信息
 */
export async function queryPRLogInfTotal(business_data_id) {
    return await window.queryPRLogInfTotal(business_data_id, tools);
}

/**
 * @function 获取登录用户
 */
export async function queryLoginUser() {
    return await window.queryLoginUser(tools);
}

/**
 * @function 获取n位随机数,随机来源chars
 */
export function queryRandomStr(length = 32) {
    return window.queryRandomStr(length, tools);
}

/**
 * @function 获取n位随机数,随机来源chars
 */
export function queryRandom(n) {
    return window.queryRandom(n, tools);
}

/**
 * @function 通过函数暴露API
 */
export function commonArgs() {
    return window.requestAPIConfig;
}

/**
 * @function 查询请假类型
 */
export function queryLeaveType(leave) {
    return (window.leaveTypeConfig || {})[leave];
}

/**
 * @function 查询流程状态
 */
export function queryBpmStatus(status) {
    return (window.bpmStatusConfig || {})[status];
}

/**
 * @function 根据表名查询表单名称
 */
export function queryFormName(tableName) {
    return window.queryFormName(tableName);
}

/**
 * @function 根据表名查询表单名称
 */
export function queryFormTypeName(tableName) {
    return window.queryFormTypeName(tableName);
}

/**
 * @function 根据表名查询表单名称
 */
export function queryFormTypeValue(tableName) {
    return window.queryFormTypeValue(tableName);
}

/**
 * @function 根据表名查询表单名称
 */
export function queryFormMainTable(tableName) {
    return window.queryFormMainTable(tableName);
}

/**
 * @function 开始日期表单显示名称
 */
export function queryFormMTStarttimeName(tableName) {
    return window.queryFormMTStarttimeName(tableName);
}

/**
 * @function 结束日期表单显示名称
 */
export function queryFormMTEndtimeName(tableName) {
    return window.queryFormMTEndtimeName(tableName);
}

/**
 * @function 结束日期表单显示名称
 */
export function queryFormMTFileName(tableName) {
    return window.queryFormMTFileName(tableName);
}

/**
 * @function 查询附表字段
 */
export function queryFormMTSubColumns(tableName) {
    return window.queryFormMTSubColumns(tableName);
}

/**
 * @function 查询附表数据
 */
export async function queryFormMTSubData(tableName, foreignKey, id) {
    return await window.queryFormMTSubData(tableName, foreignKey, id, queryTableDataByField, queryBpmStatus, tools);
}

/**
 * @function 查询用户的真实姓名
 */
export function queryUserRealName(name) {
    return window.queryUserRealName(name, queryUserNameByCache);
}

/**
 * @function 查询表单对应的历史自由流程
 */
export async function queryHisFreeWorkflow(id) {
    return await window.queryHisFreeWorkflow(id);
}

/**
 * @function 查询表单对应的历史自由流程
 */
export async function queryCurFreeWorkflow(id) {
    return await window.queryCurFreeWorkflow(id, tools);
}

/**
 * @function 获取当前自动生成的文件编号
 */
export async function queryFileNumb(companyName, tableName, date, curRow, tname) {
    return await window.queryFileNumb(companyName, tableName, date, curRow, tname, tools);
}

/**
 * @function 查询当前评论信息
 */
export async function queryCurReplayList(id) {
    return await window.queryCurReplayList(id, tools, storage, authortool);
}

/**
 * @function 查询审批流程信息
 */
export async function queryWorkflows(business_data_id) {
    return await window.queryWorkflows(business_data_id, queryPRLogHistoryByDataID, queryUserRealName, queryPRLogByDataID, queryPRLogInfByDataID, storage, tools);
}

/**
 * @function 获取某业务记录对应的审批日志信息
 */
export async function queryDepartNameByCode(code) {
    return await window.queryDepartNameByCode(code, tools);
}

/**
 * @function 查询表单详情页面
 */
export async function watchFormLeave(that) {
    return await window.watchFormLeave(that, queryFormName, queryTableData, transWflowToHistory, queryDepartNameByCode, queryWorkflows, queryLeaveType, queryFormTypeValue, queryBpmStatus, queryFormTypeName, queryFormMainTable, queryFormMTSubColumns, queryFormMTSubData, queryFormMTStarttimeName, queryFormMTEndtimeName, queryFormMTFileName, queryWorkflowStatus, queryTableFieldInfo, tools);
}

/**
 * @function 知会/流程通知转移到历史记录中
 */
export async function transWflowToHistory(tableName, id) {
    return await window.transWflowToHistory(tableName, id, queryProcessLog, queryProcessLogInformed, transWflowHistoring, tools);
}

/**
 * @function 知会/流程通知转移到历史记录的子操作
 */
export async function transWflowHistoring(tableName, wfnode) {
    //返回结果
    return await window.transWflowHistoring(tableName, wfnode, deleteProcessLog, deleteProcessLogInf, postProcessLogHistory, tools);
}

/**
 * @function 查询文档对应预览地址
 * @param {*} text
 */
export async function queryFileViewURL(text) {
    return await window.queryFileViewURL(text, tools);
}

/**
 * @function 查询文件类型
 * @description 查询表单含有的文件的文档类型
 * @param {*} text
 */
export function queryFileType(text) {
    return window.queryFileType(text, tools);
}

/**
 * @function 查询附件中的图片地址
 */
export function queryImageURL(text) {
    return window.queryImageURL(text, tools);
}

/**
 * @function 查询附件中的视频地址
 */
export function queryVideoURL(text) {
    return window.queryVideoURL(text, tools);
}

/**
 * @function 获取博文信息
 */
export async function queryBloggerInfo(username, result) {
    return await window.queryBloggerInfo(username, result, tools);
}

/**
 * @function 查询工资表单信息
 */
export async function queryWageByParam(
    username = '',
    params = '',
    page = 0,
    size = 50,
    result = ''
) {
    return await window.queryWageByParam(username, params, page, size, result, tools);
}

/**
 * @function 查询工资表单信息
 */
export async function queryWageBillByParam(
    username = '',
    params = '',
    page = 0,
    size = 50,
    result = ''
) {
    //查询工资表单信息
    return await window.queryWageBillByParam(username, params, page, size, result, tools);
}


/**
 * @function 查询工资信息
 */
export async function queryWageByUserName(
    realname = '',
    username = '',
    page = 0,
    size = 50,
    result = ''
) {
    //查询工资信息
    return await window.queryWageByUserName(realname, username, page, size, result, tools);
}


/**
 * @function 查询花名册信息
 */
export async function queryRegisterByUserName(
    realname = '',
    username = '',
    page = 0,
    size = 50,
    result = ''
) {
    //查询花名册信息
    return await window.queryRegisterByUserName(realname, username, page, size, result, tools);
}

/**
 * @function 查询工资表单信息
 */
export async function queryRegisterByParam(
    username = '',
    params = '',
    page = 0,
    size = 50,
    result = ''
) {
    //查询工资表单信息
    return await window.queryRegisterByParam(username, params, page, size, result, tools);
}

/**
 * @function 查询附件中的文档地址
 */
export async function queryOfficeURL(text) {
    return await window.queryOfficeURL(text, tools);
}

/**
 * @function 设置详情页面图片展示样式
 */
export function changeImageCSS() {
    return window.changeImageCSS();
}

/**
 * @function callback连续执行函数
 */
export function setTimeouts(callback, ...times) {
    return window.setTimeouts(callback, ...times);
}

/**
 * @function 设置css样式
 */
export function postcss() {
    //设置css样式
    return window.postcss();
}

/**
 * @function 获取当前节点是否有知会或者审批节点信息
 */
export async function queryCurNodePageType(pageType) {
    //返回pageType
    return await window.queryCurNodePageType(pageType, tools, queryProcessLogByID, queryProcessLogInfByID);
}

/**
 * @function 渲染审批流程详情
 */
export async function colorProcessDetail(that, main) {
    //返回设置结果
    return await window.colorProcessDetail(that, main, tools, queryFileType, queryFileViewURL, queryOfficeURL, queryImageURL, queryCurNodePageType, changeImageCSS);
}

/**
 * @function 查询表字段信息
 * @param {*} tableName
 */
export async function queryTableFieldInfoJSON(tableName) {
    //查询表字段信息
    return await window.queryTableFieldInfoJSON(tableName, tools, queryTableDataByField);
}

/**
 * @function 查询工作流节点状态
 */
export async function queryWorkflowStatus(tableName, id) {
    //返回节点信息
    return await window.queryWorkflowStatus(tableName, id, tools, queryTableData);
}

/**
 * @function 检测URL是否有效
 * @param {*} url
 */
export async function queryUrlValid(url) {
    //检测URL是否有效
    return await window.queryUrlValid(url);
}

/**
 * @function 查询用户总数
 */
export async function queryUserCount() {
    //查询用户总数
    return await window.queryUserCount();
}

/**
 * @function 查询流程总数
 */
export async function queryWflowCount() {
    //查询流程总数
    return await window.queryWflowCount();
}

/**
 * @function 查询月度流程数
 */
export async function queryWflowMonthCount() {
    //查询月度流程数
    return await window.queryWflowMonthCount(tools);
}

/**
 * @function 查询流程总数
 */
export async function queryWflowDayCount() {
    //查询流程总数
    return await window.queryWflowDayCount(tools);
}

/**
 * @function 查询流程日同比
 */
export async function queryWflowDailyRatio() {
    //查询流程总数
    return await window.queryWflowDailyRatio();
}

/**
 * @function 查询流程月同比
 */
export async function queryWflowMonthlyRatio() {
    //查询流程总数
    return await window.queryWflowMonthlyRatio();
}

/**
 * @function 查询月度新增用户数
 */
export async function queryNewUserTotal() {
    //查询月度新增用户数
    return await window.queryNewUserTotal();
}

/**
 * @function 查询上月月度新增用户数
 */
export async function queryNewUserTotalLastMonth() {
    //查询上月月度新增用户数
    return await window.queryNewUserTotalLastMonth();
}

/**
 * @function 查询业务数据
 */
export async function queryBusinessTotal() {
    //查询业务数据
    return await window.queryBusinessTotal();
}

/**
 * @function 获取问卷信息列表
 */
export async function queryQuestionList(username, page = 0, size = 99) {
    //获取问卷信息列表
    return await window.queryQuestionList(username, page, size);
}

/**
 * @function 获取问卷信息列表
 */
export async function queryQuestionById(id, page = 0, size = 99) {
    //获取问卷信息列表
    return await window.queryQuestionById(id, page, size);
}

/**
 * @function 将当前自由流程的数据转移到历史数据中
 * @param {*} id
 */
export async function transFreeWflowHis(id) {
    //将当前自由流程的数据转移到历史数据中
    return await workflowAPI.transFreeWflowHis(id);
}

/**
 * @function 将英文名转化为中文名
 */
export async function patchEnameCname(origin) {
    //返回中文名称列表
    return await window.patchEnameCname(origin, tools, queryUserName);
}

/**
 * @function 将中文名转化为英文名
 */
export async function patchCnameEname(origin) {
    //返回中文名称列表
    return await window.patchCnameEname(origin, tools, queryUserName);
}

//将外部引用挂载到window上
window.requestAPI = requestAPI;
window.storage = storage;
window.tools = tools;
window.workflowAPI = workflowAPI;